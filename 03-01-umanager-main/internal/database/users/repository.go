package users

import (
	"context"
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"

	"GB_Study_03/gb_golang_final/03-01-umanager-main/internal/database"
)

func New(userDB *pgx.Conn, timeout time.Duration) *Repository {
	return &Repository{userDB: userDB, timeout: timeout}
}

type Repository struct {
	userDB  *pgx.Conn
	timeout time.Duration
}

func (r *Repository) Create(ctx context.Context, req CreateUserReq) (database.User, error) {
	var u database.User

	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	//TODO: implement Create
	query := "INSERT INTO users (username,password) VALUES ($1,$2) RETURNING id;"

	row := r.userDB.QueryRow(ctx, query, req.Username, req.Password)

	err := row.Scan(&u)

	if err != nil {
		log.Fatal(err)
	}

	return u, nil
}

func (r *Repository) FindByID(ctx context.Context, userID uuid.UUID) (database.User, error) {
	var u database.User

	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	//TODO: implement FindByID
	query := "SELECT * FROM users WHERE id=$1"

	row := r.userDB.QueryRow(ctx, query, userID)

	err := row.Scan(&u)

	if err != nil {
		log.Fatal(err)
	}

	return u, nil
}

func (r *Repository) FindByUsername(ctx context.Context, username string) (database.User, error) {
	var u database.User

	ctx, cancel := context.WithTimeout(ctx, r.timeout)
	defer cancel()

	//TODO: implement FindByUsername
	query := "SELECT * FROM users WHERE username=$1"

	row := r.userDB.QueryRow(ctx, query, username)
	err := row.Scan(&u)

	if err != nil {
		log.Fatal(err)
	}

	return u, nil
}
