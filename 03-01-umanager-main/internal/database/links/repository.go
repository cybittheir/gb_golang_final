package links

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"GB_Study_03/gb_golang_final/03-01-umanager-main/internal/database"
)

const collection = "links"

func New(db *mongo.Database, timeout time.Duration) *Repository {
	return &Repository{db: db, timeout: timeout}
}

type Repository struct {
	db      *mongo.Database
	timeout time.Duration
}

func (r *Repository) Create(ctx context.Context, req CreateReq) (database.Link, error) {
	var l database.Link
	//TODO: implement Create Link

	collection := r.db.Collection(collection)
	inLink, err := collection.InsertOne(ctx, req)
	if err != nil {
		log.Fatal(err)
	}

	newID := inLink.InsertedID

	filter := bson.M{"id": bson.M{"$eq": newID.(primitive.ObjectID)}}

	err = collection.FindOne(ctx, filter).Decode(&l)
	if err != nil {
		log.Fatal(err)
	}
	return l, nil
}

func (r *Repository) FindByUserAndURL(ctx context.Context, link, userID string) (database.Link, error) {
	var l database.Link
	//TODO: implement FindByUserAndURL
	collection := r.db.Collection(collection)
	filter := []interface{}{bson.M{"url": bson.M{"$eq": link}}, bson.M{"userID": bson.M{"$eq": userID}}}

	cursor, err := collection.Find(ctx, filter)

	if err != nil {
		log.Fatal(err)
	}

	if err = cursor.All(ctx, &l); err != nil {
		log.Fatal(err)
	}

	return l, nil
}

func (r *Repository) FindByCriteria(ctx context.Context, criteria Criteria) ([]database.Link, error) {

	//TODO: implement FindByCriteria
	l := make([]database.Link, 0)
	collection := r.db.Collection(collection)
	cursor, err := collection.Find(ctx, criteria)
	if err != nil {
		log.Fatal(err)
	}

	if err = cursor.All(ctx, &l); err != nil {
		log.Fatal(err)
	}

	return l, nil
}
